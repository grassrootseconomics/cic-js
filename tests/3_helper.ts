const assert = require('assert');

const Web3 = require('web3');

import { CICRegistry } from '../src';
import { TransactionHelper } from '../src';
import { FsFileGetter } from '../src/file';

const dataPath = [__dirname + '/testdata/solidity'];
const contractRegistry = '0xea6225212005e86a4490018ded4bf37f3e772161'; // address when deploying to ganache with provided db
const getter = new FsFileGetter();

describe('helper', () => {
	it('new', () => {
		const w3 = new Web3('http://localhost:63545');

		const registry = new CICRegistry(w3, contractRegistry, 'CICRegistry', getter, dataPath);

		const helper = new TransactionHelper(w3, registry);
	});
});	
