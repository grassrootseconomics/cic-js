const assert = require('assert');

const Web3 = require('web3');

import { CICRegistry } from '../src';
import { FsFileGetter } from '../src/file';

const provider = 'http://localhost:63545';
const hashOfFoo = '2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae';
const contractRegistry = '0xea6225212005e86a4490018ded4bf37f3e772161'; // address when deploying to ganache with provided db
const dataPath = [__dirname + '/testdata/solidity'];

const tokenDeclarator = '0xEb3907eCad74a0013c259D5874AE7f22DcBcC95C';
//const contractDeployer = '0xEb3907eCad74a0013c259D5874AE7f22DcBcC95C';
const tokenOwner = '0x5567139c7a1C2977A391f51D8cA45B1D6700f5F6';

const getter = new FsFileGetter();

// TODO: mock web3
describe('registry', () => {
	it('new', async () => {
		const w3 = new Web3(provider);

		const registry = new CICRegistry(w3, contractRegistry, 'CICRegistry', getter, dataPath);
		await registry.load();
	});

	it('contract', async () => {
		const w3 = new Web3(provider);

		const registry = new CICRegistry(w3, contractRegistry, 'CICRegistry', getter, dataPath);
		await registry.load();
		const accountsIndexContract = await registry.getContractByName('AccountRegistry');
		// console.log(accountsIndexContract.address);
	});

	it('token', async () => {
		const w3 = new Web3(provider);

		const registry = new CICRegistry(w3, contractRegistry, 'CICRegistry', getter, dataPath);
		await registry.load();

		//const token = await registry.getTokenByDeclaration('TokenRegistry', tokenDeclarator, 0);
		const token = await registry.getTokenBySymbol('TokenRegistry', 'GFT');

	});

	it('addToken', async() => {
		const w3 = new Web3(provider);

		const registry = new CICRegistry(w3, contractRegistry, 'CICRegistry', getter, dataPath);
		await registry.load();

		//const token = await registry.getTokenBySymbol('TokenRegistry', 'SRF');

		const tokenContract = await registry.getContractByName('TokenRegistry', 'TokenUniqueSymbolIndex');
		const tokenAddress = await tokenContract.methods.entry(0).call();
		const token = await registry.addToken(tokenAddress);
	});

	it('declaration', async() => {
		const w3 = new Web3(provider);

		const registry = new CICRegistry(w3, contractRegistry, 'CICRegistry', getter, dataPath);
		await registry.load();

		const token = await registry.getTokenBySymbol('TokenRegistry', 'GFT');
		const contract = await registry.declaratorHelper.getTokenDeclaration('AddressDeclarator', tokenOwner, token.options.address);

	});

//	await it('load', async () => {
//		const w3 = new Web3('http://localhost:8545');
//
//		const registry = new Registry(w3, contractRegistry, abi);
//
//		let tokenCount = undefined;
//		let addressReturned = undefined;
//
//		registry.ontokensload = (n) => {
//			console.debug('ontoken', n);
//			tokenCount = n;
//			console.debug('checking tokencount', tokenCount);
//			assert(tokenCount, 3);
//		};
//		registry.onregistryload = (a) => {
//			console.debug('onregistry', a);
//			addressReturned = a;
//			console.debug('checking contract address');
//			assert(addressReturned, contractRegistry);
//		};
//
//		// never reaches then block, why?
//		await registry.load(true);
//	});
//
//	await it('contracts_list', async () => {
//		const w3 = new Web3('http://localhost:8545');
//
//		const registry = new Registry(w3, contractRegistry, abi);
//
//		let tokenCount = undefined;
//		let addressReturned = undefined;
//	
//		const list = await registry.getNetworkContracts();
//		console.log(list);
//	});


});
